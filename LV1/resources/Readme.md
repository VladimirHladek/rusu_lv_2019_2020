Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

Ispravio grešku u pozivu funkcije input(), funkcije open() i kod obje print() funkcije.

U zadatku 1 korisnik unosi podatek o broju sati i plaći pomoću input() funkcija, a pomoću int() i flout() funkcija uneseni stringovi se pretvaraju u brojeve koji se množe i ispisuju pomoću print() funkcije. Da bi se umnožak ispisao pomoću str() funkcije se pretvara u string.

U zadatak 2 korsinik unosi broj u intervalu [0, 1]. Ako je unos neispravan dobije Exception, a u suprotnom ispisuje odgovarajuću ocijenu.

U zadatku 3 se otvori zadatak 1 i prije nego što zatraži unos od korisnika definira se funkcija kojoj se predaju broj radnih sati i plaća, a dobije se njihov umnožak. U ispisu se ispisuje povratna vrijednost funkcije za unesene vrijednosti.

U zadatku 4 se prvo napravi prazna lista te se zatim ulazi u petlju koja se prekida tek kada korisnik unese string "Done". U petlji korisnik unosi brojeve koji, ako su ispravno uneseni, dodaju se listi, a u suprotnom se izbacuje Exception i prikladna poruka te se traži ponovni unos brojeva. Nakon završetka petlje ispisuju se duljina liste pomoću len(), prosjek pomoću sum() i len() te minimum i maksimum pomoću min() i max().

U zadatku 5 pomoću open() funkcije otvara se tekstna datoteka čije ime korisnik sam unosi, a zatim se prolazi kroz svaki red teksta i traže se redovi koji poćinju s određenim tekstom te se rijeći tih redova razdvoje na razmacima i stavljaju u listu gdje se uzima druga rijeć koja je broj koji se nadodaje na varijablu sum koja se na kraju podijeli s varijablom count koja predstavlja ukupni broj brojeva koji su nadodani na sum i tako se dobije prosjek.

U zadatku 6 se pomoću startswith() funkcije pronalaze funkcije s hostnameima te se pomoću split() funkcije prvo dolazi do emaila, a zatim do hostnamea. Svi hostnameovi se dodaju u listu i u set, a zatim se radi dict u koji se dodaju hostnamei iz seta i pridodaje im se broj koji predstavlja broj pojavljivanja u tekstu, a taj broj se dobije brojanjem u listi u kojoj je svaki hostname više puta napisan.