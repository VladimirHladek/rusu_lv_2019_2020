try:
    grade = float(input("Enter grade: "))
except:
    print("Grade can not be a string")

try:
    if grade < 0 or grade > 1:
        raise Exception()
    if grade >= 0.9:
        print("A")
    elif grade >= 0.8:
        print("B")
    elif grade >= 0.7:
        print("C")
    elif grade >= 0.6:
        print("D")
    else:
        print("F")
except:
    print("Please enter a grade between 0.0 and 1.0")
