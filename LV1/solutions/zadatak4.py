numbers = []

number = 0
while number != 'Done':
    number = input("Enter number: ")
    if number == 'Done':
        break
    try:
        numbers.append(float(number))
    except:
        print("Invalid number entered")

print("\n" + str(len(numbers)) + " numbers entered")
print("Average: " + str(sum(numbers) / len(numbers)))
print("Minimal: " + str(min(numbers)))
print("Maximal: " + str(max(numbers)))