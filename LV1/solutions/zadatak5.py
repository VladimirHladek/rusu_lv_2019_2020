fhand = open(input("Ime datoteke: "))

sum = 0
count = 0
for line in fhand:
    line = line.rstrip()
    if line.startswith('X-DSPAM-Confidence:'):
        sum += float(line.split()[1])
        count += 1

print("Average X-DSPAM-Confidence: " + str(sum/count))