import re

def main():
    names = []
    fhand = open('mbox-short.txt')
    for line in fhand:
        line = line.rstrip()
        name = re.findall('\S*@\S+', line)
        names = names + name
    
    for i in range(0,len(names)):
        names[i] = names[i][0:names[i].index('@')]
    
    print(names)

if __name__ == '__main__':
    main()