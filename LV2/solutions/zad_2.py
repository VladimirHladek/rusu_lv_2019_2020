import re

def main():
    names1 = []
    names2 = []
    names3 = []
    names4 = []
    names5 = []
    fhand = open('mbox-short.txt')
    for line in fhand:
        line = line.rstrip()
        name1 = re.findall('\S*a\S*@\S+', line)
        name2 = re.findall('[b-zB-Z]*a[b-zB-Z]*@\S+', line)
        name3 = re.findall('[b-zB-Z0-9]*@\S+', line)
        name4 = re.findall('\S*[0-9]+\S*@\S+', line)
        name5 = re.findall('[a-z]*@\S+', line)
        names1 = names1 + name1
        names2 = names2 + name2
        names3 = names3 + name3
        names4 = names4 + name4
        names5 = names5 + name5
    
    for i in range(0,len(names1)):
        names1[i] = names1[i][0:names1[i].index('@')]
    
    for i in range(0,len(names2)):
        names2[i] = names2[i][0:names2[i].index('@')]
    
    for i in range(0,len(names3)):
        names3[i] = names3[i][0:names3[i].index('@')]

    for i in range(0,len(names4)):
        names4[i] = names4[i][0:names4[i].index('@')]
    
    for i in range(0,len(names5)):
        names5[i] = names5[i][0:names5[i].index('@')]
    
    print(names1)

if __name__ == '__main__':
    main()