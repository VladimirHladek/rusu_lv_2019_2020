import numpy as np
import matplotlib.pyplot as plt

def getMean(array):
    return np.mean(array)

def main():
    np.random.seed(50)
    sex = np.random.randint(0,2,30)
    height = np.array([])

    
    for i in range(len(sex)):
        if sex[i] == 1:
            height = np.append(height, np.random.normal(180, 7, 1))
        else:
            height = np.append(height, np.random.normal(167, 7, 1))

    maleHeight = height*sex
    femaleHeight = height - maleHeight

    maleHeight = np.delete(maleHeight, np.where(maleHeight==0.0))
    femaleHeight = np.delete(femaleHeight, np.where(femaleHeight==0.0))
    
    plt.plot(maleHeight, np.ones(len(maleHeight)), 'o', color = 'blue')
    plt.plot(femaleHeight, np.zeros(len(femaleHeight)), 'o', color = 'red')

    maleMean = getMean(maleHeight)
    femaleMean = getMean(femaleHeight)

    plt.plot(maleMean, 1, '*', color = 'c', markersize=12)
    plt.plot(femaleMean, 0, '*', color = 'm', markersize=12)

    plt.show()



if __name__ == '__main__':
    main()