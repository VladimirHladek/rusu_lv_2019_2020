import numpy as np
import matplotlib.pyplot as plt

def main():
    np.random.seed(8)
    diceRolls = np.random.randint(1, 7, 100)
    plt.hist(diceRolls, [1, 2, 3, 4, 5, 6, 7])
    plt.axis([1, 7, 0, 30])
    plt.show()


if __name__ == '__main__':
    main()