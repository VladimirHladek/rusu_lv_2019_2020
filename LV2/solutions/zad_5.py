import pandas as pd
import matplotlib.pyplot as plt
import mplcursors

df = pd.read_csv('mtcars.csv')
mpg = df['mpg']
hp = df['hp']
wt = df['wt']

fig, ax = plt.subplots()
ax.plot(hp, mpg, 'bx')
plt.xlabel('hp')
plt.ylabel('mpg')
crs = mplcursors.cursor(ax, hover=True)

crs.connect("add", lambda sel: sel.annotation.set_text(str(wt[sel.index]*1000) + " lbs"))

print("Minimal mpg: {}".format(min(mpg)))
print("Average mpg: {}".format((sum(mpg)/len(mpg))))
print("Maximal mpg: {}".format(max(mpg)))

plt.show()