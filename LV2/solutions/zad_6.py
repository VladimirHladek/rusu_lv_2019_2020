import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

img = mpimg.imread('tiger.png')

red_data = img[:,:,0]
green_data = img[:,:,1]
blue_data = img[:,:,2]

img2 = np.array([red_data + 0.5 * (1 - red_data), green_data + 0.5 * (1 - green_data), blue_data + 0.5 * (1 - blue_data)])
img2 = np.swapaxes(img2, 0, 1)
img2 = np.swapaxes(img2, 1, 2)

imgplot = plt.imshow(img2)
plt.show()