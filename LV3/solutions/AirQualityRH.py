import urllib
import pandas as pd
import json
import requests
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np

url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/json?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.01.2017&vrijemeDo=31.12.2017'

airQualityHR = urllib.request.urlopen(url).read()
data = json.loads(airQualityHR)

for e in data:
    e['vrijeme'] = datetime.fromtimestamp(e['vrijeme']/1000)

df = pd.DataFrame(data)



# Printing the 3 days when the concetration was the highest:
df1 = df.sort_values(['vrijednost'], ascending=False).head(3)
print("The three highest concentration days:")
print(df1)



# Ploting the number of days missing in each month:
missing_days_count=[]
missing_days_count.append((datetime(2017, 2, 1)-datetime(2017, 1, 1)).days - len(df.query('20170101 <= vrijeme < 20170201')))
missing_days_count.append((datetime(2017, 3, 1)-datetime(2017, 2, 1)).days - len(df.query('20170201 <= vrijeme < 20170301')))
missing_days_count.append((datetime(2017, 4, 1)-datetime(2017, 3, 1)).days - len(df.query('20170301 <= vrijeme < 20170401')))
missing_days_count.append((datetime(2017, 5, 1)-datetime(2017, 4, 1)).days - len(df.query('20170401 <= vrijeme < 20170501')))
missing_days_count.append((datetime(2017, 6, 1)-datetime(2017, 5, 1)).days - len(df.query('20170501 <= vrijeme < 20170601')))
missing_days_count.append((datetime(2017, 7, 1)-datetime(2017, 6, 1)).days - len(df.query('20170601 <= vrijeme < 20170701')))
missing_days_count.append((datetime(2017, 8, 1)-datetime(2017, 7, 1)).days - len(df.query('20170701 <= vrijeme < 20170801')))
missing_days_count.append((datetime(2017, 9, 1)-datetime(2017, 8, 1)).days - len(df.query('20170801 <= vrijeme < 20170901')))
missing_days_count.append((datetime(2017, 10, 1)-datetime(2017, 9, 1)).days - len(df.query('20170901 <= vrijeme < 20171001')))
missing_days_count.append((datetime(2017, 11, 1)-datetime(2017, 10, 1)).days - len(df.query('20171001 <= vrijeme < 20171101')))
missing_days_count.append((datetime(2017, 12, 1)-datetime(2017, 11, 1)).days - len(df.query('20171101 <= vrijeme < 20171201')))
missing_days_count.append((datetime(2018, 1, 1)-datetime(2017, 12, 1)).days - len(df.query('20171201 <= vrijeme < 20180101')))

plt.bar(range(1,13), missing_days_count)
plt.xticks(ticks=range(1,13), labels=range(1,13))
plt.xlabel("month")
plt.ylabel("number of days missing")
plt.show()



# Compairing the concentration in January and August:
df_january = df.query('20170101 <= vrijeme < 20170201')
df_august = df.query('20170801 <= vrijeme < 20170901')

plt.boxplot([df_january['vrijednost'], df_august['vrijednost']])
plt.xticks(ticks=[1,2], labels=["January", "August"])
plt.xlabel("month")
plt.ylabel("value")
plt.show()



# Compairing the concentration on workdays and weekends:
df['weekday'] = df['vrijeme'].dt.dayofweek

df_workdays = df.query('weekday<5')
df_weekend = df.query('weekday>=5')

plt.boxplot([df_workdays['vrijednost'], df_weekend['vrijednost']])
plt.xticks(ticks=[1,2], labels=["Workdays", "Weekend"])
plt.xlabel("type")
plt.ylabel("value")
plt.show()