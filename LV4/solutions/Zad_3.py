import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

def non_func(x):
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)
plt.show()

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)
plt.show()

linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

print('Model je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')

ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)

plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)
plt.show()

"""
x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)
"""

def kriterijJ(x, y, theta):

    J = 0.0
    n = x.shape[0]

    for i in range(0,n):
        J += (predict(x[i], theta) - y[i]) ** 2
    
    J /= (2*n)

    return J

def predict(x, theta):
    return theta[0] + theta[1]*x

alfa = 0.05
num_iter = 100
theta_old = np.array([0.5, 0.5])
theta_new = np.zeros((2,1))
J = np.zeros((num_iter,1))

xp = np.array([x.min(), x.max()])
yp = np.zeros((2,1))

n_samples = len(x)

plt.figure(4)
plt.scatter(x, y_measured, marker='.')   
plt.title("gradient descent")

for iter in range(0, num_iter):

    J[iter] = kriterijJ(x,y_measured,theta_old)

    rj0 = 0.0
    rj1 = 0.0

    for i in range(0,n_samples):
        rj0 += predict(x[i],theta_old) - y_measured[i]
        rj1 += (predict(x[i],theta_old) - y_measured[i])*x[i]

    rj0 /= n_samples
    rj1 /= n_samples

    theta_new[0] = theta_old[0] - alfa * rj0
    theta_new[1] = theta_old[1] - alfa * rj1
    
    theta_old = theta_new

    yp[0] = predict(xp[0],theta_new)
    yp[1] = predict(xp[1],theta_new)
        
    plt.plot(xp,yp)
    plt.pause(0.1)

plt.figure(5)
plt.plot(range(iter+1),J)
plt.ylabel('J')
plt.xlabel('iteracija')
plt.show()

print("Theta0: ", theta_old[0])
print("Theta1: ", theta_old[1])