from funkcija_5_1 import generate_data
from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

def assignToCentar(X, centers):
    n = len(X)
    K = len(centers)

    distance = -1*np.ones((n, K))

    for j in range(0, K):
        distance[:,j] = np.linalg.norm(X-centers[j,:], axis = 1)

    b = np.argmin(distance, axis = 1)
    J = np.min(distance, axis = 1)
    J = J.sum()
    
    return b, J


def centroid(X, b, K):
    m = len(X[0])    
    centers = np.zeros((K, m))

    for i in range(0, K):
        centers[i,:] = np.sum(X[b==i,:], axis = 0)
        centers[i,:] /= np.sum( 1.0*(b==i))
    
    return centers


X = generate_data(500, 2)

plt.figure(1)
plt.title("Podaci")
plt.scatter(X[:,0], X[:,1])
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.show()

K = 3
centers = X[np.random.choice(X.shape[0], K, replace=False),:]

num_iter = 10
J = np.zeros((num_iter, 1))

for i in range(0, num_iter):
    b, J[i,0] = assignToCentar(X, centers)

    plt.clf()
    plt.scatter(X[:,0], X[:,1], c=b)
    plt.scatter(centers[:,0],centers[:,1], color = 'k', marker = '*')
    plt.title(str(i))

    centers = centroid(X, b, K)

    plt.pause(1)

plt.show()